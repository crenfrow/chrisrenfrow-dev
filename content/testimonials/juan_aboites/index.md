+++
weight = 1
[extra]
praise = "I deeply appreciate your steady and calming energy during check-ins and your overall generosity and openness. You're a valuable presence in a community like RC."
author = "Juan Aboites"
affiliation = "Peer at The Recurse Center"
+++
