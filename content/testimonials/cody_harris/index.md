+++
weight = 1
[extra]
praise = "Chris, it's been great! From the start as your onboarding buddy to the end of batch I've enjoyed your presence at RC and the enthusiasm and intellectual curiosity you exude."
author = "Cody Harris"
affiliation = "Peer at The Recurse Center"
+++
