+++
weight = 0
[extra]
praise = "Chris is kind and smart, and he will be a great addition to any team that will be wise enough to hire him!"
author = "Angel Hudgins"
affiliation = "Peer at The Recurse Center"
+++
