+++
title = "Findable"
weight = 1
[extra]
completed = true
start = 2019-07-01
end = 2020-08-01
url = "https://findable.net/about"
hero_img = "./hero.jpg"
+++

As a software developer, I assisted in building Findable, a marketplace for the sharing economy. Built using React and integrated with the Stripe API to facilitate secure transactions.

<!-- more -->
