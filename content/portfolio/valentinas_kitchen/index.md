+++
title = "Valentina's Kitchen"
description = ""
weight = 0
[extra]
# url = 
finished = false
start = 2023-02-11
# end =
+++

An informational website built for a local restaurant and catering business. Statically generated using [zola](https://getzola.org) paired with a theme of my own design styled with [TailwindCSS](https://tailwindcss.com).

<!-- more -->
