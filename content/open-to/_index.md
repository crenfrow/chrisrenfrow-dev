+++
title = "I am open to work!"
sort_by = "weight"
render = false
+++

With 5+ years of experience in software design and development, I bring a strong combination of technical skills and a passion for learning to the table. I am available for contract, freelance, or full-time positions and seek to work with companies that value a positive work culture and life-long learning. Interested? Let's [connect](/#let-s-get-in-touch)!
